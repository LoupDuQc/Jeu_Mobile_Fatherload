local mTank={}
function mTank:new(controle,jauge)
    local tank=display.newGroup()
    --infos/settings
    tank.image=display.newImage(imagePrefix.."tank.2d.png")
    tank:insert(tank.image)
    tank.speed=tuilesSize/2
    tank.speedMax=10*tank.speed
    tank.slowDown=0.95 --% de la vitesse conservé
    tank.G=tank.speed/2 --gravité
    tank.miningSpeed=tuilesSize/20
    
    tank.jetFuelMax=80
    tank.jetFuelCur=tank.jetFuelMax
    tank.jetFuelUseRate=1
    tank.jetFuelRefillRate=1
    
    --debug, affiche la cible de minage
    local vertices = { -5,-5, 5,-5, 5,5, -5,5} 
    tank.cibleMinage = display.newPolygon( 0, 0, vertices )
    --tank:insert(tank.cibleMinage) --pour l'affichage de la cible
    
    --forme physique
    local forme = {
        -tuilesHalfSize+tuilesSize/5+1,-tuilesHalfSize+1,
        tuilesHalfSize-tuilesSize/5-1,-tuilesHalfSize+1,
        tuilesHalfSize-1,-tuilesHalfSize+tuilesSize/5+1,
        tuilesHalfSize-1,tuilesHalfSize-tuilesSize/5-1,
        tuilesHalfSize-tuilesSize/5-1,tuilesHalfSize-1,
        -tuilesHalfSize+tuilesSize/5+1,tuilesHalfSize-1,
        -tuilesHalfSize+1,tuilesHalfSize-tuilesSize/5-1,
        -tuilesHalfSize+1,-tuilesHalfSize+tuilesSize/5+1,
    }
    
    physics.addBody(tank, {shape=forme, density= 10000, friction = 0.6, bounce = 0.1})
    tank.isFixedRotation=true
    
    --visuel de la drill
    tank.drill=display.newGroup()
    tank.drill0=display.newImage(imagePrefix.."drill0.2d.png")
    tank.drill1=display.newImage(imagePrefix.."drill1.2d.png")
    tank.drill2=display.newImage(imagePrefix.."drill2.2d.png")
    tank.drill3=display.newImage(imagePrefix.."drill3.2d.png")
    --insertion du visuel
    tank:insert(tank.drill)
    tank.drill:insert(tank.drill0)
    tank.drill:insert(tank.drill1)
    tank.drill:insert(tank.drill2)
    tank.drill:insert(tank.drill3)
    --placement du visuel
    local distanceCible={}--cible du placement de la drill, car la largeur du tank change avec le placement
    distanceCible.x,distanceCible.y=tank.width/2+tank.drill0.width/2,-tank.height/10
    tank.drill.x,tank.drill.y=distanceCible.x,distanceCible.y

    --cache le visuel des frames non utilisé
    tank.drill0.isVisible=false
    tank.drill1.isVisible=false
    tank.drill3.isVisible=false
    
    --states
    --tank.states={"normal","mine","mineBas"}--comme referance, des int utilisés pour les performances
    tank.state=1
    tank.tuileCible={x=0,y=0} --objet gardans la coordoné de la tuile que le tank mine
    tank.drillState=2 --frame de l'animation
	tank.drillAngle=0
	tank.drillRotationSpeed=5
	tank.lastXScale=1
    tank.isPaused=false
    
    function tank:enterFrame(e)
		
        if(self.isPaused)then
            return 
        end
    
		local deltaRotation=0
		local rotationDeg=(controle.joystick.angle/math.pi)*180
		
        if(self.state==1)then --si normal
			deltaRotation=rotationDeg-tank.drillAngle
            
            
            --horizon
            local curVitX,curVitY=self:getLinearVelocity()
            local nextVitX,nextVitesseY
            if(controle.joystick.dirX~=0)then --si joystick utilisé
                nextVitX=curVitX+tank.speed*controle.joystick.dirX
                if(nextVitX>tank.speedMax)then --limiteure de vitesse x vers la droite
                    nextVitX=tank.speedMax
                elseif(nextVitX<-tank.speedMax)then --limiteur de vitesse vers la gauche
                    nextVitX=-tank.speedMax
                end
                
            elseif(controle.joystick.dirX==0)then --si le joystick est a 0, ralentie
                if(self.parent.parent:checkAt(self.x,self.y+tuilesSize/2)~=3 and self.parent.parent:checkAt(self.x,self.y+tuilesSize/2)~=9)then --si est sur le sol pour la friction
                    nextVitX=curVitX*tank.slowDown
                else
                    nextVitX=curVitX
                end
            end
            
            --affichage de la directionen fonction du joystick
			if(math.abs(rotationDeg)>90)then --vers la gauche
                self.xScale=-1
            elseif(math.abs(rotationDeg)<90)then --vers la droite
               self.xScale=1 
            end
			

            
            --vertical
            if(controle.joystick.dirY<-0.5) then 
                if(tank.jetFuelCur>0)then
                    --self:applyForce( 0, -tank.speed*100, tank.x, tank.y )
                    nextVitY=curVitY-tank.speed*-controle.joystick.dirY --propulsion vers le haut de l'écran avec force du joystick
                    if(nextVitY<-tank.speedMax)then --limiteur
                       nextVitY=-tank.speedMax 
                    end
                    tank.jetFuelCur=tank.jetFuelCur-tank.jetFuelUseRate*-controle.joystick.dirY
                    jauge:update(tank.jetFuelCur/tank.jetFuelMax)
                else --utilise la gravité si n'a plus de carburant
                   nextVitY=curVitY+tank.G
                    if(nextVitY>tank.speedMax*2)then --limiteur
                       nextVitY=tank.speedMax*2 
                    end 
                end
            else --gravité
                nextVitY=curVitY+tank.G
                if(nextVitY>tank.speedMax*2)then --limiteur
                   nextVitY=tank.speedMax*2 
                end
                if(tank.jetFuelMax>tank.jetFuelCur)then --limiteur carburant
                    tank.jetFuelCur=tank.jetFuelCur+tank.jetFuelRefillRate --reprise du carburent a jetpack
                    jauge:update(tank.jetFuelCur/tank.jetFuelMax)
                end
                
            end
            
            self:setLinearVelocity(nextVitX,nextVitY)--update la vitesse
            
            
            --activation minage
            if(controle.mine)then --minage
				--if(tank.drill.rotation>45 and tank.drill.rotation<135)then --minage vers le bas
				if(rotationDeg>45 and rotationDeg<135)then --minage vers le bas
					if(tuilesMinableNom[tuilesNoms[(self.parent.parent:checkAt(self.x,self.y+tuilesSize/2))]])then
						self.tuileCible.x=self.x-(self.x%tuilesSize)+tuilesHalfSize --set la cible biens centré
						self.tuileCible.y=self.y+tuilesSize
						self.isBodyActive=false
						self.state=3 --self.state="mine"
					end
				else --minage vers le coté
					if(tuilesMinableNom[tuilesNoms[(self.parent.parent:checkAt(self.x+self.xScale*tuilesSize,self.y))]])then --si la tuile sur le coté est minable
						if(self.parent.parent:checkAt(self.x,self.y+tuilesSize/2)~=3 and self.parent.parent:checkAt(self.x,self.y+tuilesSize/2)~=9)then --si est sur le sol (donc la tuile en dessou n'est pas vide ni un magasin)
							self.tuileCible.x=(self.x+self.xScale*tuilesSize-(self.x+self.xScale*tuilesSize)%tuilesSize)+tuilesHalfSize
							self.isBodyActive=false
							self.state=2 --self.state="mine"
						end
					end
				end  
            end
			
			
            
        elseif(tank.state==2)then --minage sur les coté
			
			--rotation de la drill
			deltaRotation=-tank.drillAngle*tank.xScale
			
			--logique de minage
            self.cibleMinage.x=(self.x-self.tuileCible.x)*-self.xScale
            if(self.x-self.tuileCible.x<5 and self.x-self.tuileCible.x>-5)then --si a la position cible, donc fini de miné
                self.state=1 --self.state = "normal"
                self.isBodyActive=true
                self.parent.parent:mineAt(self.x,self.y)
                --reset de la drill
                self["drill"..self.drillState].isVisible=false
                self.drillState=2
                self["drill"..self.drillState].isVisible=true
                --reset de la vitesse
                self:setLinearVelocity(0,0)
            else
                --deplacement
                self.x=self.x+self.miningSpeed*self.xScale
                self["drill"..self.drillState].isVisible=false
                self.drillState=self.drillState+1
                if(self.drillState==4)then
                   self.drillState=0 
                end
                self["drill"..self.drillState].isVisible=true
            end
        else --if(tank.state==3)then --minage vers le bas
			deltaRotation=90-tank.drillAngle*tank.xScale
            self.cibleMinage.x=(self.x-self.tuileCible.x)*-self.xScale
            self.cibleMinage.y=(self.tuileCible.y-self.y)
            if(self.tuileCible.y-self.y<5)then --si a la position cible, donc fini de miné
                self.state=1 --self.state = "normal"
                self.isBodyActive=true
                self.parent.parent:mineAt(self.x,self.y)
                --reset de la drill
                self["drill"..self.drillState].isVisible=false
                self.drillState=2
                self["drill"..self.drillState].isVisible=true
                --reset de la vitesse
                self:setLinearVelocity(0,0)
            else
                --deplacement
                self.x=self.x-(self.x-self.tuileCible.x)/10
                self.y=self.y+self.miningSpeed
                self["drill"..self.drillState].isVisible=false
                self.drillState=self.drillState+1
                if(self.drillState==4)then
                   self.drillState=0 
                end
                self["drill"..self.drillState].isVisible=true
            end
        end
		
		
		--rotation relative joystick/position de la drill
		if(deltaRotation>180)then
			deltaRotation= deltaRotation-360
		elseif(deltaRotation<-180)then
			deltaRotation = deltaRotation+360
		end
		
		--si change de coté
		if(tank.xScale ~= tank.lastXScale)then 
			tank.lastXScale=tank.xScale
			tank.drillAngle=180-tank.drillAngle --mirroir de l'angle
		end
		
		
		--smoothing de la rotation
		if(deltaRotation>0)then
			if(deltaRotation>tank.drillRotationSpeed)then --si a plus d'une distance de vitesse de rotation
				tank.drillAngle=tank.drillAngle+tank.drillRotationSpeed
			else --si a moins d'une distance de rotation
				tank.drillAngle=tank.drillAngle+deltaRotation --snap
			end
		elseif(deltaRotation<0)then
			if(deltaRotation<-tank.drillRotationSpeed)then
				tank.drillAngle=tank.drillAngle-tank.drillRotationSpeed
			else
				tank.drillAngle=tank.drillAngle+deltaRotation
			end
		end

		--print(deltaRotation,tank.drillAngle)

		--rotation visuel
		tank.drill.x=math.abs(tuilesSize/1.5*math.cos(tank.drillAngle/180*math.pi))
		tank.drill.y=(tuilesSize/1.5*math.sin(tank.drillAngle/180*math.pi))
		tank.drill.rotation=tank.drillAngle


		--inversion de la rotation de la drill si le tank a le visuel inversé
		if(tank.xScale==-1)then 
			tank.drill.rotation=180-tank.drill.rotation
		end
		
    end
    
    function tank:preCollision( e )
        if(e.other.currentNom=="shop")then  --test si colision avec un magasin, si oui, désactive la colision et schédule le test pour savoir quel magasin
            local action = function()
                e.other:testSiFin()
            end
            timer.performWithDelay( 0, action)
            if(e.contact~=nil)then --fix pour un erreur rare où e.contact est nul, sans doute un bog avec le simulateur
                e.contact.isEnabled = false
            end
        end
		
		if(e.other.isColide==false)then  --test si colision avec un magasin, si oui, désactive la colision et schédule le test pour savoir quel magasin
            if(e.contact~=nil)then --fix pour un erreur rare où e.contact est nul, sans doute un bog avec le simulateur
                e.contact.isEnabled = false
            end
        end
    end
    
    function tank:updateCible(x,y)
        self.tuileCible.x=self.tuileCible.x-x*tuilesSize
        self.tuileCible.y=self.tuileCible.y-y*tuilesSize
    end
    
    function tank:pause()
        self.isPaused=true
    end
    
    function tank:unpause()
		
        self.isPaused=false
    end
    
    tank:addEventListener( "preCollision", tank )
    Runtime:addEventListener( "enterFrame", tank )
    
   return tank 
end
return mTank