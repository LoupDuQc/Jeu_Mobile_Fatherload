local mParticules={}
function mParticules:new(colide,num)
	local particules={}
    for i=1,num do
		local particule=display.newImage(imagePrefix.."fragment"..math.random(1,8)..".png")
		particules[i]=particule
		physics.addBody(particule, { density= 1, friction = 0.6, bounce = 0.3})
		particule.isColide=colide
		particule.alphaData=1.5
		
		function particule:tick()
			self.alphaData=self.alphaData-0.01
			--self.yScale=self.yScale-0.01
			self.alpha=self.alphaData
		end
		
		
	end
	return particules 
end
return mParticules