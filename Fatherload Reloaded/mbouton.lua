mBouton={}
function mBouton:new(x,y,callbackUp,callbackDown,init,imageUp,imageDown)
    local bouton = display.newGroup()
    local visuel = display.newImage("joystick_interne_a.png") --visuel temporaire
    bouton:insert(visuel)
    bouton.x=x-bouton.width/2
    bouton.y=y-bouton.height/2
    
    
    function bouton:init()
        --si les fonctions ne sont pas donné
        if(callbackDown==nil)then
            callbackDown=function()end
        end
        if(callbackUp==nil)then
            callbackUp=function()end
        end
        if(init==nil)then
            init=function()end
        end
        --init
        init()
        
    end
    
    function bouton:touch(e)
        if ( e.phase == "ended" or e.phase == "cancelled") then --up
            display.getCurrentStage():setFocus(e.target,nil)
			callbackUp()
		elseif ( e.phase == "began" ) then --down
            display.getCurrentStage():setFocus(e.target,e.id)
			callbackDown()
        end

	end
    
    bouton:init()
    bouton:addEventListener( "touch", bouton )
    return bouton
end
return mBouton
