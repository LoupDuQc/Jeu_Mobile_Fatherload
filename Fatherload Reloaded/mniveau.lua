--puisque l'utilisation d'un perlin noise requiere l'utilisation de l'evenement onColorSample, la fonction ne retourne rien, mais appele pa prochaine fonction à la place



mNiveau={}
function mNiveau:new()
    local tempsDepart=system.getTimer()
    --generation d'un array de la taille de la map avec seullement de la terre
    local niveauData={}
    for i=0,niveauHeight do
        local colone={}
        for j=0,niveauWidth do
            local id=1
            colone[j]=id
        end
        niveauData[i]=colone
    end    

    --importation du perlin noise
    local map = require("640x320") -- proche du max possible, 640x640 est trop gros
    local mapTotalWidth=640
    local mapTotalHeight=320

    niveauData.positionRandomX=math.random(mapTotalWidth-niveauWidth) --ajout d'une propriété directement dans un array, car çca marche en lua
    niveauData.positionRandomY=math.random(mapTotalHeight-niveauHeight)-- moitier de la hauteur car il faut pouvoir avoir de la place pour asser de place pour les prochain niveaux

    --prise des valeurs brute
    for i=0,niveauHeight do 
        for j=0,niveauWidth do
            niveauData[j][i]=map[(j+niveauData.positionRandomX) + ((i+niveauData.positionRandomY)*mapTotalWidth)]--position dans un array 1d qui représente un array 2d
        end
    end

    --utilisation des valeurs
    for i=0,niveauHeight do
        for j=0,niveauWidth do
            --if(i<niveauHeight) then
            if((i<niveauHeight and j ~= niveauWidth and j ~= 0 and i~=0)) then --si intérieur
            --if(j ~= niveauWidth) then
                if (niveauData[j][i]>0.5 and niveauData[j][i]<0.55) then
                    niveauData[j][i]=2
                elseif (niveauData[j][i]>0.6 and niveauData[j][i]<0.63) then
                    niveauData[j][i]=4
                elseif (niveauData[j][i]>0.7 and niveauData[j][i]<0.72) then
                    niveauData[j][i]=5
                elseif (niveauData[j][i]>0.8 and niveauData[j][i]<0.81) then
                    niveauData[j][i]=6
                elseif (niveauData[j][i]>0.9 and niveauData[j][i]<0.9025) then
                    niveauData[j][i]=7
                elseif(niveauData[j][i]>0.5) then
                    niveauData[j][i]=1
                else
                    niveauData[j][i]=3
                end
            else --si mur exterieur
               niveauData[j][i]=8 --bedrock
            end
        end
    end

    --generation des magasin
    function niveauData:generationShop(x,y)
        local materielContour=3

        niveauData[x][y]=9
        --a proximité
        niveauData[x-1][y]=materielContour
        niveauData[x+1][y]=materielContour
        niveauData[x-1][y-1]=materielContour
        niveauData[x+1][y-1]=materielContour
        niveauData[x][y-1]=materielContour
        --un peu plus loin
        niveauData[x+2][y]=materielContour
        niveauData[x+2][y-1]=materielContour
        niveauData[x+1][y-2]=materielContour
        niveauData[x][y-2]=materielContour
        niveauData[x-1][y-2]=materielContour
        niveauData[x-2][y-1]=materielContour
        niveauData[x-2][y]=materielContour
        --en dessous
        niveauData[x-1][y+1]=8
        niveauData[x+1][y+1]=8
        niveauData[x][y+1]=8
    end



    --magasin top
    local posShopX=math.random(niveauWidth-16)+8
    local posShopY=math.random(4)+4
    niveauData:generationShop(posShopX,posShopY)
    niveauData.magasinHaut={posShopX,posShopY}

    --magasin bottom
    posShopX=math.random(8,niveauWidth-8)
    posShopY=niveauHeight - math.random(4,6) 
    niveauData:generationShop(posShopX,posShopY)
    niveauData.magasinBas={posShopX,posShopY}

    --fin generation magasin
    
    niveauData.offset=0

    --temps requis pour la generation
    tempGeneration=(system.getTimer() - tempsDepart)
	map = nil
    return niveauData
end






function mNiveau:next(niveauData,garde)
    --import du perlin noise 
    local map = require("640x320") -- proche du max possible, 640x640 est trop gros
    local mapTotalWidth=640
    local mapTotalHeight=320
    
    --print(niveauHeight-garde)
    niveauData.offset=niveauHeight-garde
    
    --shift l'ancien bas du niveau pour le nouveau haut
    for i=0,garde do 
        for j=0,niveauWidth do
            niveauData[j][i]=niveauData[j][i + niveauHeight-garde]
        end
    end
    
    
    --placement de la roche au top du nouveau niveau
    local seedTopX = math.random(mapTotalWidth-niveauWidth)
    local seedTopY = math.random(mapTotalHeight-niveauHeight)
    
    function tourneEnBedrock(event)
        --print( event )
        niveauData[event.source.laTuileX][event.source.laTuileY]=8
        self:update()
    end
    
    for j=0,niveauWidth do
        niveauData[j][0]=8 --premier layer toujours en bedrock
        if(map[(j+seedTopX) + ((seedTopY)*mapTotalWidth)]<0.25)then --get de la valeur dans le perlin noise pour si 2 bedrock doivent etre ajoutés
            local tuileA = timer.performWithDelay( 500, tourneEnBedrock)
                tuileA.laTuileX=j
                tuileA.laTuileY=1
            local tuileB = timer.performWithDelay( 1000, tourneEnBedrock)
                tuileB.laTuileX=j
                tuileB.laTuileY=2
            local tuileC = timer.performWithDelay( 1500, tourneEnBedrock)
                tuileC.laTuileX=j
                tuileC.laTuileY=3
        elseif(map[(j+seedTopX) + ((seedTopY)*mapTotalWidth)]<0.5)then --get de la valeur dans le perlin noise pour si 2 bedrock doivent etre ajoutés
            local tuileA = timer.performWithDelay( 500, tourneEnBedrock)
            tuileA.laTuileX=j
            tuileA.laTuileY=1
            local tuileB = timer.performWithDelay( 1000, tourneEnBedrock)
            tuileB.laTuileX=j
            tuileB.laTuileY=2
        elseif(map[(j+seedTopX) + ((seedTopY)*mapTotalWidth)]<0.75)then ---get de la valeur dans le perlin noise pour si 1 bedrock doit etre ajouté
            local tuile = timer.performWithDelay( 500, tourneEnBedrock)
            tuile.laTuileX=j
            tuile.laTuileY=1
        end
    end
    
    
    --generation des nouvelles tuiles dans le bas de la map
    
     for i=garde+1,niveauHeight do 
        for j=0,niveauWidth do
            niveauData[j][i]=map[(j+niveauData.positionRandomX) + ((i+niveauData.positionRandomY+ niveauData.offset)*mapTotalWidth)]
        end
    end
    
     --utilisation des valeurs
    for i=garde+1,niveauHeight do
        for j=0,niveauWidth do
            --if(i<niveauHeight) then
            if((i<niveauHeight and j ~= niveauWidth and j ~= 0 and i~=0)) then --si intérieur
            --if(j ~= niveauWidth) then
                if (niveauData[j][i]>0.5 and niveauData[j][i]<0.55) then
                    niveauData[j][i]=2
                elseif (niveauData[j][i]>0.6 and niveauData[j][i]<0.63) then
                    niveauData[j][i]=4
                elseif (niveauData[j][i]>0.7 and niveauData[j][i]<0.72) then
                    niveauData[j][i]=5
                elseif (niveauData[j][i]>0.8 and niveauData[j][i]<0.81) then
                    niveauData[j][i]=6
                elseif (niveauData[j][i]>0.9 and niveauData[j][i]<0.9025) then
                    niveauData[j][i]=7
                elseif(niveauData[j][i]>0.5) then
                    niveauData[j][i]=1
                else
                    niveauData[j][i]=3
                end
            else --si mur exterieur
               niveauData[j][i]=8 --bedrock
            end
        end
    end
    
    --magasin
	niveauData.magasinHaut={niveauData.magasinBas[1],niveauData.magasinBas[2]}
    posShopX=math.random(niveauWidth-16)+8
    posShopY=niveauHeight - math.random(4,6)
    niveauData:generationShop(posShopX,posShopY)
    niveauData.magasinBas={posShopX,posShopY}
	
	
	--.toExplode={x=niveauData.magasinHaut[1],y=garde} --explose en dessous du magasin pour que çca sois visible
	niveauData.toExplode={x=niveauData.magasinHaut[1]+math.random(-3,3),y=garde} --explose en dessous du magasin pour que çca sois visible
	

    
	map=nil
    
end


return mNiveau

