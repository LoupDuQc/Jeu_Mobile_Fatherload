--settings
textureType="x1"

tuilesNoms={"dirt","rock","background","copper","iron","silver","gold","bedrock","shop"}
tuilesSolideNoms={}
    tuilesSolideNoms.dirt=true
    tuilesSolideNoms.rock=true
    tuilesSolideNoms.background=false
    tuilesSolideNoms.copper=true
    tuilesSolideNoms.iron=true
    tuilesSolideNoms.silver=true
    tuilesSolideNoms.gold=true
    tuilesSolideNoms.bedrock=true
    tuilesSolideNoms.shop=true
tuilesMinableNom={}
    tuilesMinableNom.dirt=true
    tuilesMinableNom.rock=false
    tuilesMinableNom.background=false
    tuilesMinableNom.copper=true
    tuilesMinableNom.iron=true
    tuilesMinableNom.silver=true
    tuilesMinableNom.gold=true
    tuilesMinableNom.bedrock=false
    tuilesMinableNom.shop=false
tuilesMinableValeurNom={}
    tuilesMinableValeurNom.dirt=0
    tuilesMinableValeurNom.rock=0
    tuilesMinableValeurNom.background=0
    tuilesMinableValeurNom.copper=20
    tuilesMinableValeurNom.iron=55
    tuilesMinableValeurNom.silver=150
    tuilesMinableValeurNom.gold=500
    tuilesMinableValeurNom.bedrock=false
    tuilesMinableValeurNom.shop=false



niveauWidth=32
niveauHeight=48

--variables global
if(textureType=="x2" or display.contentWidth > 48 * niveauWidth or display.contentWidth > 48 * niveauHeight) then --scaling des textures, seullement pour le debug en zoom-out ou adaptif en cas que l'écran soit trop gros
    tuilesSize=96
    tuilesHalfSize=48
    imagePrefix="96/96x"
elseif (textureType=="x1") then
    tuilesSize=48
    tuilesHalfSize=24
    imagePrefix="48/48x"
end

niveauHeight=niveauHeight --pour le chargement du magasin
niveauWidth=niveauWidth