--physique
local physics = require("physics")
physics.start()
--physics.setDrawMode( "hybrid" )
system.activate( "multitouch" )



--machines
local stats = require("stats")
local machineAffichage = require("maffichage")

local machineTank = require("mtank")
local machineControle = require("mcontrole")
local machineJauge = require("mjauge")


--creation d'objets
local jaugeBig = mJaugeFuel:new("big")
local jaugeSmall = mJaugeFuel:new("small")
local controle = machineControle.new()
local tank = machineTank:new(controle,jaugeSmall)
local affichage = machineAffichage:new(controle,tank,jaugeSmall,jaugeBig)

local jeu = display.newGroup()

jeu:insert(affichage)
jeu:insert(controle)

