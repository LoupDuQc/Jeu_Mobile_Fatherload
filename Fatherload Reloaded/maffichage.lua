mAffichage={}
function mAffichage:new(joystick,tank,jaugeSmall,jaugeBig)
    local machineTuile = require("mtuile")
    local machineNiveau = require("mniveau")
    local machineParticules = require("mparticules")
    local affichage=display.newGroup()
    local data = machineNiveau:new() --la map
	

	
	
    function affichage:init()
        --informations
        self.quantiteTuilesX=math.ceil(display.contentWidth/tuilesSize+4)-- +1 serait normalement assez, mais le 4+ est requis pour que le magasin charge hors de la vue et les connected textures hor écran
        self.quantiteTuilesY=math.ceil(display.contentHeight/tuilesSize+4)

        self.largeurTier=display.contentWidth/3
        self.largeurDeuxTier=(display.contentWidth*2)/3
        self.hauteurDeuxTier=(display.contentHeight*2)/3
        self.hauteurTier=(display.contentHeight)/3
        
        --info temps
        self.tempsMax=3600--temps en frame par niveau
        self.tempsCur=0
        
        --mode
        --local modes={"normal","nextLevel","shake"}
        self.mode=1
        self.tickAnimation=0
        
        --insertion des objets
        self.tuiles=display.newGroup()
        self:insert(self.tuiles)
        self:insert(joystick)
        self.listeTuiles={}
        
        self.offsetX=0--offset de l'affichage par rapport au données
        self.offsetY=0
        
        --générations des tuiles
        for i=0,self.quantiteTuilesY do
            local colone={}
            for j=0,self.quantiteTuilesX do
                local tuile=machineTuile:new(j,i)
                colone[j]=tuile
                self.tuiles:insert(tuile)
                --tuile:affiche(data[j][i])
            end
            self.listeTuiles[i]=colone
        end
        
        for i=0,self.quantiteTuilesX do
            for j=0,self.quantiteTuilesY do
                local tuile= self.listeTuiles[j][i]
                tuile:checkSiMagasin()
            end
        end
        
        --texte d'information a propos du temps requis pour le deplacement de la grille de tuile
        self.myText = display.newText("nil", display.contentWidth/2,display.contentHeight /2, display.contentWidth, display.contentHeight/3, native.systemFont , 16  )
        self.myText:setFillColor( 1, 1, 1 )
        self:insert(self.myText)
        --self.memText = display.newText("nil", display.contentWidth/2,display.contentHeight /2, display.contentWidth, display.contentHeight/4, native.systemFont , 16  )
        --self.memText:setFillColor( 1, 1, 1 )


        --placement de la grille de tuile
        self.tuiles.x=self.tuiles.x-tuilesSize
        self.tuiles.y=self.tuiles.y-tuilesSize
        
        
        --placement du tank
        self.tuiles:insert(tank)
        tank.x=data.magasinHaut[1]*tuilesSize
        tank.y=data.magasinHaut[2]*tuilesSize+tank.height/2
        
        --placement des jauges
        self:insert(jaugeSmall)
        self:insert(jaugeBig)
        jaugeBig.x=display.contentWidth-jaugeBig.width/1.5
        jaugeBig.y=jaugeBig.height*1.5
        jaugeSmall.x=jaugeBig.x+jaugeBig.width/2-jaugeSmall.width/2
        jaugeSmall.y=jaugeBig.y+jaugeBig.height
        
        --pointage
        self.scoreDisplay = display.newText("nil", display.contentWidth/2,display.contentHeight/2, native.systemFont , 16  )
        self.scoreDisplay:setFillColor( 1, 1, 1 ,0)
        self:insert(self.scoreDisplay)
		
		--debris
		self.particules={}
		self.maxParticules=48
        
		function self:shift(X,Y)
			self.offsetX=self.offsetX-X
			self.offsetY=self.offsetY-Y
			tank.y=tank.y+tuilesSize*Y
			tank.x=tank.x+tuilesSize*X
			tank:updateCible(-X,-Y)

			for i=1,#self.particules do
				self.particules[i].x=self.particules[i].x+tuilesSize*X
				self.particules[i].y=self.particules[i].y+tuilesSize*Y

				self.tuiles:insert(self.particules[i])
			end
			self:update()

		end

        function self:enterFrame(e)
            -- creation des variables
            local prochainePosX = self.tuiles.x
            local prochainePosY = self.tuiles.y

            
            if(self.mode==2)then --si doit shaker
				--shake
                prochainePosX=self.tuiles.x+math.cos(self.tickAnimation/3)*tuilesSize/8
                prochainePosY=self.tuiles.y+math.sin(self.tickAnimation)*tuilesSize/16
                self.tickAnimation=self.tickAnimation+1
				--timer qui remonte
				self.tempsCur=self.tempsCur-30
				if(self.tempsCur<0)then --limite
					self.tempsCur=0
				end
				system.vibrate()
            else --Mouvements avec le tank--
                
				--get des valeurs requises
                local posTankX,posTankY=tank:localToContent(0,0)


                --follow du tank sur les x
                if(posTankX>self.largeurDeuxTier) then --si le tank est dans la partie droite de l'écrant,
                    prochainePosX=self.tuiles.x-(posTankX-self.largeurDeuxTier)/3
                elseif(posTankX<self.largeurTier) then --si le tank est dans la partie gauche de l'écrant,
                    prochainePosX=self.tuiles.x+(self.largeurTier-posTankX)/3
                end

                --follow du tank sur les y
                if(posTankY>self.hauteurDeuxTier) then --si le tank est dans la partie bas de l'écrant,
                    prochainePosY=self.tuiles.y-(posTankY-self.hauteurDeuxTier)/3
                elseif(posTankY<self.hauteurTier) then --si le tank est dans la partie haut de l'écrant,
                    prochainePosY=self.tuiles.y+(self.hauteurTier-posTankY)/3
                end
				
				--timer
	            self.tempsCur=self.tempsCur+1
				
				if(self.tempsCur>self.tempsMax)then --limite temporaire
					self.tempsCur=self.tempsMax
				end

                
            end
			            

            --gestion de la grille dans les Y
            if(prochainePosY>-tuilesSize) then --deplacement Y de la camera avec limiteur
                if(self.offsetY==0) then --si au bord de la map
                    if(prochainePosY>0) then
                        prochainePosY=0
                    end
                else
                    prochainePosY=prochainePosY-tuilesSize
					self:shift(0,1)
                end
            elseif(prochainePosY<-2*tuilesSize)then
                if(self.offsetY==niveauHeight-self.quantiteTuilesY) then --si au bord de la map
                    if(prochainePosY<=-tuilesSize*5-(display.contentHeight%tuilesSize)*2) then
                        prochainePosY=-tuilesSize*5-(display.contentHeight%tuilesSize)*2
                    end
                else
                    prochainePosY=prochainePosY+tuilesSize
					self:shift(0,-1)
                end
            end
            
            
            --gestion de la grille dans les X
            if(prochainePosX>-tuilesSize) then --deplacement X de la camera avec limiteur
                if(self.offsetX==0) then --si au bord de la map
                    if(prochainePosX>0) then
                        prochainePosX=0
                    end
                else
                    prochainePosX=prochainePosX-tuilesSize
					self:shift(1,0)
                end
            elseif(prochainePosX<-2*tuilesSize)then 
                if(self.offsetX==niveauWidth-self.quantiteTuilesX) then --si au bord de la map
                    if(prochainePosX<=-5*tuilesSize-(display.contentWidth%tuilesSize)*2) then
                        prochainePosX=-5*tuilesSize-(display.contentWidth%tuilesSize)*2
                    end  
                else
					prochainePosX=prochainePosX+tuilesSize
					self:shift(-1,0)
                end
            end
            
            --applique ce qui a été calculé
            self.tuiles.x=prochainePosX
            self.tuiles.y=prochainePosY
            
            
            
            --Timer--
            jaugeBig:update((self.tempsMax-self.tempsCur)/self.tempsMax)
			
			
			--tick des debris
			for i=1,#self.particules do
				self.particules[i]:tick()
			end
			
			if((#self.particules ~=0 and self.particules[1].alpha<=0)or #self.particules > self.maxParticules)then
				display.remove(self.particules[1])
				table.remove (self.particules ,1)
			end
			
            
        end --fin enterFrame

		
        function self:update()
            local tempsDepart=system.getTimer() --debug, calcule le temps prit pour chaque update de la map
            for i=0,self.quantiteTuilesX do
                for j=0,self.quantiteTuilesY do
                    self.listeTuiles[j][i]:affiche(data[i+self.offsetX][j+self.offsetY]) --set ce que la tuile affiche
                end
            end
            self.tuiles:insert(tank) --remet le tank au top, en cas de magasin
            local tempUpdate=(system.getTimer() - tempsDepart)
            self.myText.text=tempUpdate --debug
        end
        
		
        function self:checkAt(x,y)
           return data[math.floor(x/tuilesSize)+self.offsetX][math.floor(y/tuilesSize)+self.offsetY]
        end

		
        function self:mineAt(x,y)
            print(data[math.floor(x/tuilesSize)+self.offsetX][math.floor(y/tuilesSize)+self.offsetY])
            data[math.floor(x/tuilesSize)+self.offsetX][math.floor(y/tuilesSize)+self.offsetY]=3
            self:update()
			
			--debris
			local fragments=machineParticules:new(true,4)
			
			for i=1,4 do
				self.tuiles:insert(fragments[i])
				fragments[i].x=x
				fragments[i].y=y
				table.insert(self.particules,fragments[i])
			end
			
			
			self:update()
			--placement du tank par dessu les debris
			
        end
		
		--EXPLOSION!!!
		function self:explosion(posX,posY)
			print("EXPLOSION!")
			
			function explose(x,y)
				data[x][y]=3

				--debris
				local fragments=machineParticules:new(true,3)

				for k=1,3 do 
					self.tuiles:insert(fragments[k])
					fragments[k].x=(x-self.offsetX)*tuilesSize
					fragments[k].y=(y-self.offsetY)*tuilesSize
					table.insert(self.particules,fragments[k])
				end
			end
			
			for i=-1,1  do --milieux
				for j=-2,2 do
					explose(j+posX,i+posY)
				end
			end
			
			--top
			explose(posX-1,posY-2)
			explose(posX,posY-2)
			explose(posX+1,posY-2)
			--bas
			explose(posX-1,posY+2)
			explose(posX,posY+2)
			explose(posX+1,posY+2)
			
		end
        
		
        function self:tentativeProchainNiveau(x,y)
            
            function endAnimation()
				--logique
                self.rotation=0
                tank:unpause()
                self.mode=1
                self.tickAnimation=0
				
            end
            
            if(self.mode==1)then
                local cibleY=data.magasinBas[2]--get la position Y du magasin
                if(math.floor(y/tuilesSize)+self.offsetY==cibleY)then --comparaison entre la position absolue de la tuile shop en colision avec la position dans la map
                    tank:pause()
                    self.mode=2
                    timer.performWithDelay( 2500, endAnimation)
                    local offset=-3
                    local garde = self.quantiteTuilesY+offset
                    machineNiveau.next(self,data,garde)
                    tank.y=tank.y+tuilesSize*offset
                    self.tuiles.y=self.tuiles.y-tuilesSize*offset
                    self.offsetY=0
                    self:explosion(data.toExplode.x,data.toExplode.y)
					self:update()
                end
            end
        end
        
        
        
        
    end
    
    
    affichage:init()
    affichage:update()
    Runtime:addEventListener( "enterFrame", affichage )
    return affichage
end
return mAffichage
