mControle={}
function mControle:new()
    local machineJoystick = require("mjoystick")
    local machineBouton = require("mbouton")
    local controle = display.newGroup()
    
    function controle:init()

        controle.joystick = machineJoystick:new()
        controle:insert(controle.joystick)
        
        controle.boutonMine=machineBouton:new(
            display.contentWidth-tuilesSize,
            display.contentHeight-tuilesSize,
            function()self.mine=false end,
            function()self.mine=true end,
            function()self.mine=false end,
            display.newImage("joystick_interne_a.png")
        )
        controle:insert(controle.boutonMine)
        
        
        
    end
    controle:init()
    
    return controle
end
return mControle