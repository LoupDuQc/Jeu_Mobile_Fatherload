mJaugeFuel={}
function mJaugeFuel:new(taille)
    local jauge=display.newGroup()
    if(taille==nil)then
        print("la taille de la jauge n'a pas été déclarée")
        return {}
    elseif(taille=="big")then
        jauge.bordure=display.newImage(imagePrefix.."fuel_big.2d.png")
        jauge.fuelVisuel=display.newImage(imagePrefix.."fuel_inside_big.2d.png")
    elseif(taille=="small")then
        jauge.bordure=display.newImage(imagePrefix.."fuel_small.2d.png")
        jauge.fuelVisuel=display.newImage(imagePrefix.."fuel_inside_small.2d.png")
    else
        print("mauvaise taille de jauge de carburent : "..taille)
        return {} 
    end
    
    --visuel non variable
    jauge.fuel=display.newGroup()
    jauge.fuel:insert(jauge.fuelVisuel)
    jauge:insert(jauge.fuel)
    jauge:insert(jauge.bordure)
    jauge.fuel.x=jauge.fuelVisuel.width/2
    jauge.fuelVisuel.x=jauge.fuelVisuel.width/2
    jauge.fuel.xScale=-1 --pour que le visuel se rapetisse vers le bord de l'écran, donc vers la droite
    
    function jauge:update(quantite) --de 0 (vide) à 1 (plein)
        if(quantite<0)then
            quantite=0
        end
        self.fuel.xScale=-quantite-0.01 --pour ne pas set le scale à 0
        if(quantite<0)then
            
        end
    end
    
    return jauge
end
return mJaugeFuel