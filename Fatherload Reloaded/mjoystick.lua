mJoystick={}
function mJoystick:new()
    
    local joystick = display.newGroup()
    --local joystick = display.newImage("joystick.png")
    
    function joystick:init()
        --visuel
        self.mainframe = display.newImage("joystick_mainframe_v2.png")
        self:insert(self.mainframe)
        self.stick = display.newGroup()
        self:insert(self.stick)
        self.stick.stickMain = display.newImage("joystick_interne_a.png")
        self.stick.stickSlave = display.newImage("joystick_interne_b.png")
        self.stick:insert(self.stick.stickMain)
        self.stick:insert(self.stick.stickSlave)
        --setting
        self.zoneActivation=96  --largeur de la bordure visuel
        --utilitaire
        self.y = display.contentHeight-self.height/2 - tuilesSize --1 tuile du bas
        self.x=self.width/2+tuilesSize --1 tuile du bord
        self.dirX=0
        self.dirY=0
		self.angle=0
        
    end
    
    function joystick:touch(e)
        
        --affichage du joystick
        local posRelativeX,posRelativeY=self:contentToLocal(e.x,e.y)
		self.angle=math.atan2(posRelativeY,posRelativeX)
		--X
		local modX = math.abs(math.cos(self.angle))
		self.stick.x=posRelativeX*0.6
        --limite
        if(self.stick.x>self.mainframe.width/5*modX) then
            self.stick.x=self.mainframe.width/5*modX
        elseif(self.stick.x<-self.mainframe.width/5*modX)then
            self.stick.x=-self.mainframe.width/5*modX
        end
        self.stick.stickSlave.x=self.stick.x*0.15 --paralax
		
        --Y
		local modY = math.abs(math.sin(self.angle))
        self.stick.y=posRelativeY*0.6
        --limite
        if(self.stick.y>self.mainframe.height/5*modY) then
            self.stick.y=self.mainframe.height/5*modY
        elseif(self.stick.y<-self.mainframe.height/5*modY)then
            self.stick.y=-self.mainframe.height/5*modY
        end
        self.stick.stickSlave.y=self.stick.y*0.15 --paralax
		
		
		
        --logique force
		
		--transformation a une valeur de -1 a 1
            self.dirX = e.x-self.x
            self.dirX=self.dirX/self.zoneActivation
			self.dirY = e.y-self.y
            self.dirY=self.dirY/self.zoneActivation
            if(self.dirX>1) then
                self.dirX=1
            elseif(self.dirX<-1) then
                self.dirX=-1
            end
			if(self.dirY>1) then
                self.dirY=1
            elseif(self.dirY<-1) then
                self.dirY=-1
            end
        
        --down/up
		if ( e.phase == "ended" ) then --tous à off
			self.dirX=0
            self.dirY=0
            self.up=false
            self.down=false
            self.stick.x=0
            self.stick.stickSlave.x=0
			self.stick.y=0
            self.stick.stickSlave.y=0
            display.getCurrentStage():setFocus(e.target,nil)
        elseif(e.phase == "began")then
            display.getCurrentStage():setFocus(e.target,e.id)
		end
		

	end
    
    joystick:init()
    joystick:addEventListener( "touch", joystick )
    return joystick
end
return mJoystick
