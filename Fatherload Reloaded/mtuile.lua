local mTuile={}

function mTuile:new(posX,posY)--position relative a la camera
    local tuile=display.newGroup()
    tuile.posX=posX
    tuile.posY=posY
    tuile.currentNom="dirt"
   
    --préchargement de chaque état possible
    --configs
    --local visualOffsetX=math.floor(tuilesSize*0.2317/2)
    local visualOffsetX=0
    local visualOffsetY=math.floor(tuilesSize*0.1375)
    
    --importation
    tuile.dirt=display.newImage(imagePrefix.."dirt.2d.png")
    tuile.copper=display.newImage(imagePrefix.."copper.2d.png")
    tuile.iron=display.newImage(imagePrefix.."iron.2d.png")
    tuile.silver=display.newImage(imagePrefix.."silver.2d.png")
    tuile.gold=display.newImage(imagePrefix.."gold.2d.png")
    tuile.rock=display.newImage(imagePrefix.."rock.2d.png")
    tuile.background=display.newImage(imagePrefix.."background.2d.png")
    tuile.bedrock=display.newImage(imagePrefix.."bedrock.2d.png")
    tuile.shop=display.newImage(imagePrefix.."shop.2d.png")
    --insertion
    tuile:insert(tuile.dirt)
    tuile:insert(tuile.copper)
    tuile:insert(tuile.iron)
    tuile:insert(tuile.silver)
    tuile:insert(tuile.gold)
    tuile:insert(tuile.rock)
    tuile:insert(tuile.background)
    tuile:insert(tuile.bedrock)
    tuile:insert(tuile.shop)
    --desactivation des images
    --tuile.dirt.isVisible=false
    tuile.copper.isVisible=false
    tuile.iron.isVisible=false
    tuile.silver.isVisible=false
    tuile.gold.isVisible=false
    tuile.rock.isVisible=false
    tuile.bedrock.isVisible=false
    tuile.background.isVisible=false
    tuile.shop.isVisible=false
    --fin préchargement
    
    --replacement des tuiles avec textures connecté
    tuile.dirt.x,tuile.dirt.y=visualOffsetX,visualOffsetY
    tuile.copper.x,tuile.copper.y=visualOffsetX,visualOffsetY
    tuile.iron.x,tuile.iron.y=visualOffsetX,visualOffsetY
    tuile.silver.x,tuile.silver.y=visualOffsetX,visualOffsetY
    tuile.gold.x,tuile.gold.y=visualOffsetX,visualOffsetY
    tuile.rock.x,tuile.rock.y=visualOffsetX,visualOffsetY
    tuile.bedrock.x,tuile.bedrock.y=visualOffsetX,visualOffsetY
    
    --placement shop
    tuile.shop.y=-tuilesHalfSize
   
    
    --positionement
    tuile.x=(posX*tuilesSize+tuilesHalfSize)
    tuile.y=(posY*tuilesSize+tuilesHalfSize)
    
    --physique
    local forme = { -tuilesHalfSize,-tuilesHalfSize,  tuilesHalfSize,-tuilesHalfSize,  tuilesHalfSize,tuilesHalfSize,  -tuilesHalfSize,tuilesHalfSize }
    physics.addBody(tuile, "static" , {shape=forme,density= 10, friction = 0.3, bounce = 0.2})
    
    --fonctions
    function tuile:affiche(id) --utilisé lors de l'update de la map, sois qu'une tuile est changée, soit a cause du scrolling
        
        if(id~=3)then --si n'est pas vide, se remet sur le dessu
           self.parent:insert(self)  
        end
        
        if(tuilesNoms[id]==self.currentNom)then --check si affiche deja la bonne tuile
          return 
        end
        
        self[self.currentNom].isVisible=false --5 fois plus rapide que de tous les rendre invisible sans garder en memoire lequel est affiché
        self[tuilesNoms[id]].isVisible=true
        self.currentNom=tuilesNoms[id]
        --check si colision requise        
        if(tuilesSolideNoms[self.currentNom])then
            self.isBodyActive = true
        else
            self.isBodyActive = false
        end
        
        
        
    end
    
    function tuile:checkSiMagasin()
        if(self.currentNom=="shop") then
            self.parent:insert(self) 
        end
    end
    
    function tuile:testSiFin()
        self.parent.parent:tentativeProchainNiveau(self.x,self.y)
    end
    
    --return
    return tuile
    
end
return mTuile
